import sys
import random, string

# word generation from ascii symbols 
def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))

def main(argv):
    file_name = argv[1]
    lines_count = int(argv[2])
    
    # opening file from argv[1]
    with open(file_name, 'w') as f:
        for i in range(lines_count):
            # for generation word length i use normal distribution
            # 10 words in one string
            line = ' '.join(randomword(int(random.normalvariate(30, 20))) for j in range(10)) + '\n'
            f.write(line)

if __name__ == "__main__":
    main(sys.argv)
