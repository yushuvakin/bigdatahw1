import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

// reducer implementation
public class LengthReducer
        extends Reducer<UIntWritable, Text, UIntWritable, Text> {
    private Text maxWord = new Text();

    public void reduce(UIntWritable key, Iterable<Text> values, Context context) {
        Iterator<Text> it = values.iterator();

        // comparing key with maximum length
        if (key.get() > maxWord.getLength())
            // refreshing longest word
            maxWord.set(it.next());
    }

    protected void cleanup(Context context
    ) throws IOException, InterruptedException {
        // writing in context length and longest word
        context.write(new UIntWritable(maxWord.getLength()), maxWord);
    }
}