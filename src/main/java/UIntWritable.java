import org.apache.hadoop.io.IntWritable;

// unsigned int implementation
public class UIntWritable
        extends IntWritable {
    public UIntWritable() {
        this.set(0);
    }

    public UIntWritable(int value) {
        this.set(value);
    }

    @Override
    public void set(int value) {
        // getting absolute value of integer
        int absValue = Math.abs(value);
        super.set(absValue);
    }
}