import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

// mapper implementation
public class LengthMapper
        extends Mapper<Object, Text, UIntWritable, Text> {
    private Text word = new Text();
    private UIntWritable wordLength = new UIntWritable();

    public void map(Object key, Text value, Context context
    ) throws IOException, InterruptedException {
        // value represents one line of a file
        StringTokenizer itr = new StringTokenizer(value.toString());

        // iterating by words in stringTokenizer
        while (itr.hasMoreTokens()) {
            String strWord = itr.nextToken();
            // check strWord contains only ascii symbols
            if (!strWord.matches("\\A\\p{ASCII}*\\z")) continue;
            // key is ascii word
            word.set(strWord);
            // value is word length
            wordLength.set(word.getLength());
            context.write(wordLength, word);
        }
    }
}