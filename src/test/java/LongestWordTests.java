import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class LongestWordTests {
    @Mock
    Mapper.Context mapperContext;

    @Mock
    Reducer.Context reducerContext;

    LengthMapper mapper;
    LengthReducer reducer;

    // set up test
    @Before
    public void setUp() {
        // initialization of mapper
        mapper = new LengthMapper();

        // initialization of reducer
        reducer = new LengthReducer();
    }

    @Test
    public void testMapper() throws IOException, InterruptedException {
        String inputLine = "a as asd asdf";
        mapper.map(new Text(), new Text(inputLine), mapperContext);
        // checking that context invoked 4 times
        verify(mapperContext, times(4)).write(new UIntWritable(4), new Text("asdf"));
    }

    @Test
    public void testReducer() throws IOException, InterruptedException {
        // invoking reduce 4 times
        reducer.reduce(new UIntWritable(1), new ArrayList<Text>() {{add(new Text("a"));}}, reducerContext);
        reducer.reduce(new UIntWritable(2), new ArrayList<Text>() {{add(new Text("as"));}}, reducerContext);
        reducer.reduce(new UIntWritable(3), new ArrayList<Text>() {{add(new Text("asd"));}}, reducerContext);
        reducer.reduce(new UIntWritable(4), new ArrayList<Text>() {{add(new Text("asdf"));}}, reducerContext);
        reducer.cleanup(reducerContext);

        // checking longest word equals "asdf" after cleanup
        verify(reducerContext).write(new UIntWritable(4), new Text("asdf"));
    }
}
